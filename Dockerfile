FROM elixir:1.10-slim AS builder

ARG APP_NAME=tavern_keeper
ARG APP_VSN=0.1.0
ARG MIX_ENV=prod

ENV APP_NAME=${APP_NAME} \
    APP_VSN=${APP_VSN} \
    MIX_ENV=${MIX_ENV}

WORKDIR /opt/app

RUN apt-get update -y && \
    apt-get install git build-essential curl  make gcc libc-dev -y && \
    mix local.rebar --force && \
    mix local.hex --force

COPY . /opt/app/tavern-keeper-api

WORKDIR /opt/app/tavern-keeper-api

RUN mix do deps.get, deps.compile, compile

RUN \
    mkdir -p /opt/built && \
    mix release && \
    cp -r _build/${MIX_ENV}/rel/${APP_NAME} /opt/built

# From this line onwards, we're in a new image, which will be the image used in production
FROM debian:buster-slim

EXPOSE 80 

ARG APP_NAME=tavern_keeper

RUN apt-get update -y && \
    apt-get install -y \
    bash \
    openssl \
    libssl-dev 


ENV REPLACE_OS_VARS=true \
    DB_POOL=10 \ 
    APP_NAME=${APP_NAME} \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8

WORKDIR /opt/app

ENTRYPOINT []

COPY --from=builder /opt/built .
COPY --from=builder /opt/app/tavern-keeper-api/priv ./opt/app/tavern_keeper/priv

CMD trap 'exit' INT; /opt/app/${APP_NAME}/bin/${APP_NAME} start
