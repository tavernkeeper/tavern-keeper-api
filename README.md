# TavernKeeper

TavernKeeper API is a GraphQL/REST API, built to support the [TavernKeeper Roleplaying Community](https://tavern-keeper.com), written in [Elixir](https://elixir-lang.org) and deployed as a container. 

Interusted in helping? Checkout the [Wiki](https://gitlab.com/tavernkeeper/tavern-keeper-api/-/wikis/Development-and-Getting-Started)

