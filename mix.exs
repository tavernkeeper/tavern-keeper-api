defmodule TavernKeeper.MixProject do
  use Mix.Project

  def project do
    [
      build_path: build_path(Mix.env()),
      app: :tavern_keeper,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {TavernKeeper.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies build path per enviornment, to avoid issues with
  # OTP Versions, Elixir Versions, and OS Versions
  def build_path(:prod), do: "_build"

  def build_path(_) do
    [
      "_build",
      :os.type() |> Tuple.to_list() |> Enum.join("-"),
      System.otp_release(),
      System.version()
    ]
    |> Enum.join("/")
  end

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.14"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_ecto, "~> 4.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},

      # Storage
      {:ecto_sql, "~> 3.1"},
      {:postgrex, ">= 0.0.0"},
      {:redix, ">= 0.0.0"},
      {:argon2_elixir, "~> 2.0"},

      # Absinthe GraphQL Client
      {:absinthe, "~> 1.4"},
      {:absinthe_plug, "~> 1.4"},
      {:absinthe_ecto, "~> 0.1"},

      # Tesla HTTP Client
      {:tesla, "~> 1.3.0"},
      {:hackney, "~> 1.15.2"},

      # Bamboo Email Client
      {:bamboo, "~> 1.4"},

      # Testing Helpers
      {:excoveralls, "~> 0.10", only: :test},
      {:ex_machina, "~> 2.3", only: [:test, :dev]}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.drop --quiet", "ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
