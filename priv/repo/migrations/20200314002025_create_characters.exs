defmodule TavernKeeper.Repo.Migrations.CreateCharacters do
  use Ecto.Migration

  def change do
    create table(:characters, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :old_id, :integer
      add :name, :string
      add :concept, :string
      add :active, :boolean, default: false, null: false
      add :user_id, :integer
      add :campaign_id, :integer
      add :quote, :string
      add :nickname, :string
      add :system_id, :integer
      add :npc, :boolean, default: false, null: false
      add :permission_id, :integer
      add :data, :map

      timestamps()
    end
  end
end
