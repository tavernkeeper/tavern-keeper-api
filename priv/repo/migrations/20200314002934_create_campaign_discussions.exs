defmodule TavernKeeper.Repo.Migrations.CreateCampaignDiscussions do
  use Ecto.Migration

  def change do
    create table(:campaign_discussions, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :old_id, :integer
      add :campaign_id, :uuid
      add :user_id, :uuid
      add :name, :string
      add :content, :text
      add :active, :boolean, default: false, null: false

      timestamps()
    end
  end
end
