defmodule TavernKeeper.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :old_id, :integer
      add :name, :citext
      add :email, :citext
      add :password_digest, :string
      add :last_activity, :utc_datetime
      add :title_id, :integer
      add :email_confirmed, :boolean, default: false, null: false
      add :data, :map

      timestamps()
    end

    create(unique_index(:users, [:email]))
    create(unique_index(:users, [:name]))
  end
end
