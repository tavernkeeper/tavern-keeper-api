defmodule TavernKeeper.Repo.Migrations.CreateRoleplays do
  use Ecto.Migration

  def change do
    create table(:roleplays, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :old_id, :integer
      add :name, :string
      add :public, :boolean, default: false, null: false
      add :user_id, :uuid
      add :campaign_id, :uuid
      add :active, :boolean, default: false, null: false

      timestamps()
    end
  end
end
