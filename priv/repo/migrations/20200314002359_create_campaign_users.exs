defmodule TavernKeeper.Repo.Migrations.CreateCampaignUsers do
  use Ecto.Migration

  def change do
    create table(:campaign_users, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :old_id, :integer
      add :user_id, :uuid
      add :campaign_id, :uuid
      add :status, :integer
      add :pinned, :boolean, default: false, null: false

      timestamps()
    end
  end
end
