defmodule TavernKeeper.Repo.Migrations.CreateCampaigns do
  use Ecto.Migration

  def change do
    create table(:campaigns, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :old_id, :integer
      add :name, :string
      add :user_id, :integer
      add :active, :boolean, default: false, null: false
      add :group_wiki, :boolean, default: false, null: false
      add :system_id, :integer
      add :looking_for_player, :boolean, default: false, null: false
      add :permission_id, :integer
      add :system_name_override, :string
      add :next_game_at, :utc_datetime
      add :max_players, :integer
      add :game_format_text, :string
      add :maturity_rating_id, :integer
      add :auto_approve, :boolean, default: false, null: false
      add :data, :map
      add :genre_id, :integer

      timestamps()
    end
  end
end
