defmodule TavernKeeper.Repo.Migrations.EnableExtentions do
  use Ecto.Migration

  def up do
    execute "CREATE EXTENSION IF NOT EXISTS citext;"
  end

  def down do
  end
end
