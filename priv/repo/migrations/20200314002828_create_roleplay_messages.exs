defmodule TavernKeeper.Repo.Migrations.CreateRoleplayMessages do
  use Ecto.Migration

  def change do
    create table(:roleplay_messages, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :old_id, :integer
      add :content, :text
      add :character_id, :uuid
      add :user_id, :uuid
      add :roleplay_id, :uuid
      add :roll, :map
      add :name_override, :string
      add :reroll_count, :integer

      timestamps()
    end
  end
end
