defmodule TavernKeeper.Repo.Migrations.CreateCampaignDiscussionComments do
  use Ecto.Migration

  def change do
    create table(:campaign_discussion_comments, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :old_id, :integer
      add :campaign_discussion_id, :uuid
      add :user_id, :uuid
      add :content, :text

      timestamps()
    end
  end
end
