# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :tavern_keeper,
  ecto_repos: [TavernKeeper.Repo]

# Configures the endpoint
config :tavern_keeper, TavernKeeper.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "i1/CI/QgMp7l+yRsVK3VV5+uX4updB86BTFTgMnA+t7vVvL2Z5OhskAeDx+lLRvJ",
  render_errors: [view: TavernKeeper.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TavernKeeper.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "ACSABQmG"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :tavern_keeper, :auth_redis, System.get_env() |> Map.get("REDIS_HOST", "localhost")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
