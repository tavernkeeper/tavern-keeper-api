use Mix.Config
env = System.get_env()

# Configure your database
config :tavern_keeper, TavernKeeper.Repo,
  username: Map.get(env, "DB_USERNAME", "postgres"),
  password: Map.get(env, "DB_PASSWORD", "postgres"),
  database: Map.get(env, "DB_NAME", "tavern-keeper-test"),
  hostname: Map.get(env, "DB_HOST", "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :tavern_keeper, TavernKeeper.Web.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :argon2_elixir,
  t_cost: 1,
  m_cost: 8
