FROM elixir:1.10-slim AS builder

WORKDIR /opt/app

RUN apt-get update -y && \
    apt-get install git build-essential curl inotify-tools -y && \
    mix local.rebar --force && \
    mix local.hex --force

CMD ["mix", "do", "deps.get,", "ecto.create,", "ecto.migrate,", "phx.server"]
