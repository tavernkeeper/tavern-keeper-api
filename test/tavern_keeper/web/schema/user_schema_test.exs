defmodule TavernKeeper.Web.UserSchemaTest do
  use TavernKeeper.Web.ConnCase

  setup context do
    {conn, user} = login(context.conn)

    context
    |> Map.put(:conn, conn)
    |> Map.put(:user, user)
  end

  describe "QUERY - User Me" do
    test "success", %{conn: conn, user: user} do
      query = """
      {
        user(id: "me") {
          name
          email
        }
      }

      """

      resp = post(conn, "/api", %{query: query})
      body = json_response(resp, 200)
      assert body["data"]["user"]["email"] == user.email
    end
  end

  describe "QUERY - User By ID" do
    test "success", %{conn: conn, user: user} do
      query = """
      {
        user(id: "#{user.id}") {
          name
          email
        }
      }

      """

      resp = post(conn, "/api", %{query: query})
      body = json_response(resp, 200)
      assert body["data"]["user"]["email"] == user.email
    end

    test "bad id", %{conn: conn} do
      query = """
      {
        user(id: "00000000-0000-0000-0000-000000000000") {
          name
          email
        }
      }

      """

      resp = post(conn, "/api", %{query: query})
      body = json_response(resp, 200)
      assert resp.status == 200
      assert is_nil(body["data"]["user"])

      assert List.first(body["errors"])["message"] ==
               "User ID 00000000-0000-0000-0000-000000000000 not found"
    end
  end
end
