defmodule TavernKeeper.Web.HealthControllerTest do
  use TavernKeeper.Web.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert json_response(conn, 200)["data"]["name"] == "tavern_keeper"
  end
end
