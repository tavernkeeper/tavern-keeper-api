defmodule TavernKeeper.Web.UserControllerTest do
  use TavernKeeper.Web.ConnCase

  setup context do
    user = TavernKeeper.UserFactory.insert(:user)

    context
    |> Map.put(:user, user)
  end

  describe "POST /users" do
    test "failure", %{conn: conn, user: user} do
      response = post(conn, "/users", %{user: Map.from_struct(user)})
      assert response.status == 422
    end

    test "success", %{conn: conn} do
      user = :user |> TavernKeeper.UserFactory.build() |> Map.from_struct()

      response = post(conn, "/users", %{user: user})
      assert response.status == 201
      data = json_response(response, 201)
      assert data["data"]["email"] == user.email
    end
  end
end
