defmodule TavernKeeper.Web.LoginControllerTest do
  use TavernKeeper.Web.ConnCase

  setup context do
    user = TavernKeeper.UserFactory.insert(:user)

    context
    |> Map.put(:user, user)
  end

  describe "POST /login" do
    test "success", %{conn: conn, user: user} do
      conn = post(conn, "/login", %{email: user.email, password: "password"})
      assert json_response(conn, 201)["data"]["user"]["email"] == user.email
    end

    test "failure", %{conn: conn, user: user} do
      conn = post(conn, "/login", %{email: user.email, password: "bad-password"})
      assert json_response(conn, 400)
    end
  end
end
