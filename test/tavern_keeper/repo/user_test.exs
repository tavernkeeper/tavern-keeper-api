defmodule TavernKeeper.Repo.UserTest do
  alias TavernKeeper.Repo.User
  use TavernKeeper.DataCase, async: true

  describe "user schema" do
    test "email validation works" do
      user =
        User.changeset(%User{}, %{
          name: "user-email-test",
          email: "no-one",
          password: "password",
          password_confirmation: "password"
        })

      assert user.valid? == false
      assert user.errors == [email: {"has invalid format", [validation: :format]}]

      user =
        User.changeset(%User{}, %{
          name: "user-email-test",
          email: "no-one@somewhere",
          password: "password",
          password_confirmation: "password"
        })

      assert user.valid? == false
      assert user.errors == [email: {"has invalid format", [validation: :format]}]

      user =
        User.changeset(%User{}, %{
          name: "user-email-test",
          email: "no-one@something.com",
          password: "password",
          password_confirmation: "password"
        })

      assert user.valid?
    end

    test "email unique validation works" do
      user = TavernKeeper.UserFactory.insert(:user)

      new_user =
        User.changeset(%User{}, %{
          name: "user-email-unique-test",
          email: user.email,
          password: "password",
          password_confirmation: "password"
        })

      {status, changeset} = TavernKeeper.Repo.insert(new_user)

      assert status == :error
      assert changeset.valid? == false
    end
  end

  test "update without password" do
    user = TavernKeeper.UserFactory.insert(:user)

    new_user =
      User.changeset(user, %{
        name: "user-update-without-password",
        email: user.email
      })

    {status, user} = TavernKeeper.Repo.update(new_user)

    assert status == :ok
    assert user.name == "user-update-without-password"
  end
end
