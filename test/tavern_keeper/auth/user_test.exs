defmodule TavernKeeper.Auth.UserTest do
  alias TavernKeeper.Auth.User
  use TavernKeeper.DataCase, async: true

  setup context do
    user = TavernKeeper.UserFactory.insert(:user)

    context
    |> Map.put(:user, user)
  end

  describe "signup/4" do
    test "is success with correct info" do
      {status, user} =
        User.signup(
          "signup-email-success-test@place.com",
          "signup-email-success-test",
          "password",
          "password"
        )

      assert status == :ok
      assert user.email == "signup-email-success-test@place.com"
    end

    test "is fails with bad info", %{user: user} do
      {status, changeset} =
        User.signup(
          "signup-email-success-test@place",
          "signup-email-success-test",
          "password",
          "password"
        )

      assert status == :error

      {status, changeset} =
        User.signup(user.email, "signup-email-success-test", "password", "password")

      assert status == :error

      {status, changeset} =
        User.signup("signup-email-success-test@place.com", user.name, "password", "password")

      assert status == :error

      {status, changeset} =
        User.signup(
          "signup-email-success-test@place.com",
          "signup-email-success-test",
          "password",
          "password-bad"
        )

      assert status == :error
    end
  end

  describe "authenticate/2" do
    test "it succeeds with correct credentials", %{user: user} do
      {status, string, login} = User.authenticate(user.email, "password")
      assert status == :ok
      assert user.id == login.id
    end

    test "it fails with incorrect credentials", %{user: user} do
      {status, string, login} = User.authenticate(user.email, "wrong-password")
      assert status == :error
    end

    test "it locks out after 5 attempts", %{user: user} do
      {status, string, login} = User.authenticate(user.email, "wrong-password")
      {status, string, login} = User.authenticate(user.email, "wrong-password")
      {status, string, login} = User.authenticate(user.email, "wrong-password")
      {status, string, login} = User.authenticate(user.email, "wrong-password")
      {status, string, login} = User.authenticate(user.email, "wrong-password")
      {status, string, login} = User.authenticate(user.email, "wrong-password")
      {status, string, login} = User.authenticate(user.email, "password")
      assert status == :error
    end
  end
end
