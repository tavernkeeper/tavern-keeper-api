defmodule TavernKeeper.Auth.PlugTest do
  use TavernKeeper.Web.ConnCase
  alias TavernKeeper.Auth.Plug

  setup context do
    {conn, user} = login(context.conn)

    context
    |> Map.put(:user_conn, conn)
    |> Map.put(:user, user)
  end

  describe "init/1" do
    test "works" do
      assert Plug.init(true)
    end
  end

  describe "call/2" do
    test "with user", %{user_conn: conn, user: user} do
      Plug.call(conn, true)
    end

    test "without user", %{conn: conn, user: user} do
      Plug.call(conn, true)
    end
  end
end
