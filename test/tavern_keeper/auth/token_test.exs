defmodule TavernKeeper.Auth.TokenTest do
  alias TavernKeeper.Auth.Token
  use TavernKeeper.DataCase, async: true

  setup context do
    user = TavernKeeper.UserFactory.insert(:user)

    context
    |> Map.put(:user, user)
  end

  describe "fetch_user/1" do
    test "it succeeds with correct token", %{user: user} do
      {:ok, token, _} = Token.generate_token({:ok, user})

      fetched = Token.fetch_user(token)

      assert fetched.id == user.id
    end

    test "it fails with incorrect token", %{user: user} do
      value = Token.fetch_user("FAIL")
      assert is_nil(value)
    end
  end
end
