defmodule TavernKeeper.Test.Login do
  def login(conn) do
    user = TavernKeeper.UserFactory.insert(:user)
    {:ok, token, user} = TavernKeeper.Auth.User.authenticate(user.email, "password")

    conn = Plug.Conn.put_req_header(conn, "authorization", "Bearer #{token}")
    {conn, user}
  end
end
