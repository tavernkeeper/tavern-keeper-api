defmodule TavernKeeper.UserFactory do
  # with Ecto
  use ExMachina.Ecto, repo: TavernKeeper.Repo

  def user_factory do
    %TavernKeeper.Repo.User{
      name: sequence(:name, &"User #{&1}"),
      email: sequence(:email, &"email-#{&1}@example.com"),
      password_digest: Argon2.hash_pwd_salt("password"),
      password: "password",
      password_confirmation: "password"
    }
  end
end
