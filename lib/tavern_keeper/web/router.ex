defmodule TavernKeeper.Web.Router do
  use TavernKeeper.Web, :router

  pipeline :graphql do
    plug :accepts, ["json"]
    plug TavernKeeper.Auth.Plug, []
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TavernKeeper.Web do
    pipe_through :api

    get "/", HealthController, :show
    post "/login", LoginController, :create
    resources "/users", UserController, only: [:show, :create]
  end

  scope "/" do
    pipe_through :graphql
    forward "/api", Absinthe.Plug, schema: TavernKeeper.Web.Schema
  end
end
