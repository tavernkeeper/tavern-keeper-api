defmodule TavernKeeper.Web.LoginController do
  use TavernKeeper.Web, :controller
  alias TavernKeeper.Auth.User
  require Logger

  @spec create(Plug.Conn.t(), map) :: Plug.Conn.t()
  def create(conn, %{"email" => email, "password" => password}) do
    case User.authenticate(email, password) do
      {:ok, token, user} ->
        conn
        |> put_status(:created)
        |> render("show.json", %{token: token, user: user})

      _ ->
        conn
        |> put_status(:bad_request)
        |> put_view(TavernKeeper.Web.ErrorView)
        |> render("400.json")
    end
  end
end
