defmodule TavernKeeper.Web.HealthController do
  use TavernKeeper.Web, :controller
  require Logger

  @build_time DateTime.utc_now()
  @version Mix.Project.config()[:version]
  @name Mix.Project.config()[:app]
  @env Mix.env()

  @spec show(Plug.Conn.t(), map) :: Plug.Conn.t()
  def show(conn, _) do
    health = %{name: @name, version: @version, build_time: @build_time, env: @env}

    Logger.info(fn ->
      "Health Check: #{@name}-#{@version}-#{DateTime.to_string(@build_time)}-#{@env}"
    end)

    conn
    |> render("show.json", health)
  end
end
