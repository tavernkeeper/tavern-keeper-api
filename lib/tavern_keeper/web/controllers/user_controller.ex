defmodule TavernKeeper.Web.UserController do
  use TavernKeeper.Web, :controller
  alias TavernKeeper.Auth.User
  require Logger

  @spec create(Plug.Conn.t(), map) :: Plug.Conn.t()
  def create(conn, %{"user" => user}) do
    case User.signup(user["email"], user["name"], user["password"], user["password_confirmation"]) do
      {:ok, user} ->
        conn
        |> put_status(:created)
        |> render("show.json", user: user)

      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> put_view(TavernKeeper.Web.ErrorView)
        |> render("422.json", changeset: changeset)
    end
  end
end
