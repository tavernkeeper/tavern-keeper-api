defmodule TavernKeeper.Web.Schema do
  use Absinthe.Schema

  alias TavernKeeper.Web.Resolvers

  import_types(__MODULE__.User)

  query do
    @desc "Get User"
    field :user, :user do
      arg(:id, non_null(:string))

      resolve(&Resolvers.Accounts.find_user/3)
    end
  end

  mutation do
  end
end
