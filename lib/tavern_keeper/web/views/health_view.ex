defmodule TavernKeeper.Web.HealthView do
  use TavernKeeper.Web, :view
  @moduledoc false

  def render("show.json", %{version: version, name: name, env: env}) do
    %{
      data: %{
        name: name,
        version: version,
        env: env
      }
    }
  end

end
