defmodule TavernKeeper.Web.LoginView do
  use TavernKeeper.Web, :view
  @moduledoc false

  def render("show.json", %{user: user, token: token}) do
    %{
      data: %{
        token: token,
        user: %{
          name: user.name,
          email: user.email
        }
      }
    }
  end
end
