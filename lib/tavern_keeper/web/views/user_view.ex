defmodule TavernKeeper.Web.UserView do
  use TavernKeeper.Web, :view
  @moduledoc false

  def render("show.json", %{user: user}) do
    %{
      data: %{
        name: user.name,
        email: user.email
      }
    }
  end
end
