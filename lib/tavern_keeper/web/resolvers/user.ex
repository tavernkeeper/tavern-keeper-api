defmodule TavernKeeper.Web.Resolvers.Accounts do
  require Logger

  def find_user(_parent, %{id: "me"}, %{context: %{current_user: user}}) do
    Logger.info("Find User - Me")
    {:ok, user}
  end

  def find_user(_parent, %{id: id}, _resolution) do
    Logger.info("Find User - ID: #{id}")

    case TavernKeeper.Account.User.find_user(id) do
      nil ->
        {:error, "User ID #{id} not found"}

      user ->
        {:ok, user}
    end
  end
end
