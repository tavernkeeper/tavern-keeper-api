defmodule TavernKeeper.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      TavernKeeper.Repo,
      # Start the endpoint when the application starts
      TavernKeeper.Web.Endpoint,
      # Starts a worker by calling: TavernKeeper.Worker.start_link(arg)
      # {TavernKeeper.Worker, arg},
      {Redix, name: :auth_store, host: Application.get_env(:tavern_keeper, :auth_redis)}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TavernKeeper.Supervisor]
    status = Supervisor.start_link(children, opts)
  end

  def migrate? do
    System.get_env()
    |> Map.get("MIGRATE_DB")
    |> is_nil
    |> Kernel.==(false)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    TavernKeeper.Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
