defmodule TavernKeeper.Repo do
  use Ecto.Repo,
    otp_app: :tavern_keeper,
    adapter: Ecto.Adapters.Postgres
end
