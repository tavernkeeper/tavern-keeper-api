defmodule TavernKeeper.Auth.User do
  @moduledoc """
    Authentication Module for Users
  """
  alias TavernKeeper.Repo
  alias TavernKeeper.Account.User
  require Logger

  @doc """
    Accepts a username and password and returns a tuple containing the following

    * a status atom of :ok or :error
    * a bearer token used for authenticaiton if the login is sucessful
    * the user if the login is successful
  """
  @spec authenticate(String.t(), String.t()) ::
          {:ok | :error, Token.t(), Repo.User.t() | nil}
  def authenticate(email, password) do
    email
    |> fetch_user
    |> checkpw(password)
    |> cast
    |> TavernKeeper.Auth.Token.generate_token()
  end

  @doc """
    Accepts an email, username, and password + confirmation and returns the following

    * a status atom of :ok or :error
    * the persisted user, or an ecto changeset in the event of an error.
  """
  @spec signup(String.t(), String.t(), String.t(), String.t()) ::
          {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def signup(email, username, password, password_confirmation) do
    %Repo.User{}
    |> Repo.User.changeset(%{
      email: email,
      name: username,
      password: password,
      password_confirmation: password_confirmation
    })
    |> Repo.insert()
    |> cast
  end

  @spec fetch_user(String.t()) :: Repo.User.t() | nil
  defp fetch_user(email) do
    Repo.User
    |> Repo.get_by(email: email)
  end

  defp cast(%Repo.User{} = user) do
    %User{
      id: user.id,
      email: user.email,
      name: user.name
    }
  end

  defp cast({:ok, %Repo.User{} = user}) do
    {:ok, cast(user)}
  end

  defp cast(arg), do: arg

  @spec checkpw(Repo.User.t(), String.t()) ::
          {:ok, map} | {:error, String.t()}
  defp checkpw(%Repo.User{} = user, password) do
    user
    |> check_count()
    |> Argon2.check_pass(password, hash_key: :password_digest, hide_user: true)
    |> log_attempt(user)
  end

  defp checkpw(nil, _), do: {:error, "Bad Login"}

  @spec check_count(Repo.User.t()) :: Repo.User.t()
  defp check_count(user) do
    if fetch_count(user) > 5 do
      Logger.warn("User Lockout: #{user.email}")
      nil
    else
      user
    end
  end

  defp fetch_count(%Repo.User{id: id}) do
    [_, _, _, [attempts, _]] =
      Redix.pipeline!(:auth_store, [
        ["MULTI"],
        ["INCR", "lockout/#{id}"],
        ["EXPIRE", "lockout/#{id}", 600],
        ["EXEC"]
      ])

    attempts
  end

  # Logs Result of login - warning on failure.
  @spec log_attempt({:ok | :error, any}, map) :: {:ok, map} | {:error, String.t()}
  defp log_attempt({:error, message}, user) do
    Logger.warn(fn -> "Login Failed: #{user.email} - #{message}" end)
    {:error, "Bad Login"}
  end

  defp log_attempt({:ok, _}, user) do
    Logger.info(fn -> "Login Success: #{user.email}" end)
    Redix.command(:auth_store, ["DEL", "lockout/#{user.id}"])
    {:ok, user}
  end
end
