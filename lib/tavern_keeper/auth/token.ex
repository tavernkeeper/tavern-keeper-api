defmodule TavernKeeper.Auth.Token do
  alias TavernKeeper.Repo

  def generate_token({:ok, user}) do
    token = Ecto.UUID.generate()

    Redix.pipeline!(:auth_store, [
      ["MULTI"],
      ["SET", "token/#{token}", user.id],
      ["EXPIRE", "token/#{token}", 604_800],
      ["EXEC"]
    ])

    {:ok, token, user}
  end

  def generate_token({:error, message}) do
    {:error, message, nil}
  end

  def fetch_user(token) do
    id =
      :auth_store
      |> Redix.pipeline!([["GET", "token/#{token}"]])
      |> List.first()

    if is_nil(id) do
      nil
    else
      Repo.get(Repo.User, id)
    end
  end
end
