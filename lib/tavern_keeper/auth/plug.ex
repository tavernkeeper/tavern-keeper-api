defmodule TavernKeeper.Auth.Plug do
  @doc """
    Plug that authori
  """
  @behaviour Plug

  require Logger

  import Plug.Conn

  alias TavernKeeper.Auth.Token

  def init(opts), do: opts

  def call(conn, _) do
    case build_context(conn) do
      {:error, message} ->
        Logger.debug(message)

        conn
        |> send_resp(:unauthorized, "")
        |> halt

      context ->
        Absinthe.Plug.put_options(conn, context: context)
    end
  end

  @doc """
  Return the current user context based on the authorization header
  """
  def build_context(conn) do
    with ["Bearer " <> token] <- get_req_header(conn, "authorization"),
         {:ok, current_user} <- authorize(token) do
      %{current_user: current_user}
    else
      _ -> {:error, "no token present."}
    end
  end

  defp authorize(token) do
    case Token.fetch_user(token) do
      nil -> {:error, "bad token"}
      user -> {:ok, user}
    end
  end
end
