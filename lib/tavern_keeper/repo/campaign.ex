defmodule TavernKeeper.Repo.Campaign do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "campaigns" do
    field :active, :boolean, default: false
    field :auto_approve, :boolean, default: false
    field :data, :map
    field :game_format_text, :string
    field :genre_id, :integer
    field :group_wiki, :boolean, default: false
    field :looking_for_player, :boolean, default: false
    field :maturity_rating_id, :integer
    field :max_players, :integer
    field :name, :string
    field :next_game_at, :utc_datetime
    field :permission_id, :integer
    field :system_id, :integer
    field :system_name_override, :string
    field :user_id, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(campaign, attrs) do
    campaign
    |> cast(attrs, [
      :name,
      :user_id,
      :active,
      :group_wiki,
      :system_id,
      :looking_for_player,
      :permission_id,
      :system_name_override,
      :next_game_at,
      :max_players,
      :game_format_text,
      :maturity_rating_id,
      :auto_approve,
      :data,
      :genre_id
    ])
    |> validate_required([
      :name,
      :user_id,
      :active,
      :group_wiki,
      :system_id,
      :looking_for_player,
      :permission_id,
      :system_name_override,
      :next_game_at,
      :max_players,
      :game_format_text,
      :maturity_rating_id,
      :auto_approve,
      :data,
      :genre_id
    ])
  end
end
