defmodule TavernKeeper.Repo.User do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{
          data: map,
          email: String.t(),
          email_confirmed: boolean,
          last_activity: DateTime.t(),
          name: String.t(),
          password_digest: String.t(),
          password: String.t(),
          password_confirmation: String.t()
        }

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "users" do
    field :data, :map
    field :email, :string
    field :email_confirmed, :boolean, default: false
    field :last_activity, :utc_datetime
    field :name, :string
    field :password_digest, :string
    field :title_id, :integer
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [
      :name,
      :email,
      :last_activity,
      :title_id,
      :email_confirmed,
      :password,
      :password_confirmation
    ])
    |> put_pass_hash
    |> validate_confirmation(:password, message: "does not match password")
    |> validate_format(:email, ~r/.+@.+\..+/)
    |> validate_required([:email, :password_digest, :name])
    |> downcase(:email)
    |> unique_constraint(:email)
    |> unique_constraint(:name)
  end

  defp put_pass_hash(%Ecto.Changeset{changes: %{password: password}} = changeset) do
    changeset
    |> put_change(:password_digest, Argon2.hash_pwd_salt(password))
  end

  defp put_pass_hash(changeset), do: changeset

  defp downcase(%Ecto.Changeset{valid?: false} = changeset, _), do: changeset

  defp downcase(%Ecto.Changeset{valid?: true} = changeset, key) do
    {_, data} = fetch_field(changeset, key)
    put_change(changeset, key, String.downcase(data || ""))
  end
end
