defmodule TavernKeeper.Repo.Roleplay.Message do
  use Ecto.Schema
  import Ecto.Changeset
  @primary_key {:id, Ecto.UUID, autogenerate: true}

  schema "roleplay_messages" do
    field :character_id, Ecto.UUID
    field :content, :string
    field :name_override, :string
    field :reroll_count, :integer
    field :roleplay_id, Ecto.UUID
    field :roll, :map
    field :user_id, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [
      :content,
      :character_id,
      :user_id,
      :roleplay_id,
      :roll,
      :name_override,
      :reroll_count
    ])
    |> validate_required([
      :content,
      :character_id,
      :user_id,
      :roleplay_id,
      :roll,
      :name_override,
      :reroll_count
    ])
  end
end
