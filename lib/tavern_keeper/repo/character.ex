defmodule TavernKeeper.Repo.Character do
  use Ecto.Schema
  import Ecto.Changeset
  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "characters" do
    field :active, :boolean, default: false
    field :campaign_id, Ecto.UUID
    field :concept, :string
    field :data, :map
    field :name, :string
    field :nickname, :string
    field :npc, :boolean, default: false
    field :permission_id, :integer
    field :quote, :string
    field :system_id, :integer
    field :user_id, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(character, attrs) do
    character
    |> cast(attrs, [
      :name,
      :concept,
      :active,
      :user_id,
      :campaign_id,
      :quote,
      :nickname,
      :system_id,
      :npc,
      :permission_id,
      :data
    ])
    |> validate_required([
      :name,
      :concept,
      :active,
      :user_id,
      :campaign_id,
      :quote,
      :nickname,
      :system_id,
      :npc,
      :permission_id,
      :data
    ])
  end
end
