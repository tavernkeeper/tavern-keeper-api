defmodule TavernKeeper.Repo.Roleplay do
  use Ecto.Schema
  import Ecto.Changeset
  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "roleplays" do
    field :active, :boolean, default: false
    field :campaign_id, Ecto.UUID
    field :name, :string
    field :public, :boolean, default: false
    field :user_id, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(roleplay, attrs) do
    roleplay
    |> cast(attrs, [
      :name,
      :public,
      :user_id,
      :campaign_id,
      :last_character_id,
      :post_count,
      :character_count,
      :active
    ])
    |> validate_required([
      :name,
      :public,
      :user_id,
      :campaign_id,
      :last_character_id,
      :post_count,
      :character_count,
      :active
    ])
  end
end
