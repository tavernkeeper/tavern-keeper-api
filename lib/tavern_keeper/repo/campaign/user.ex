defmodule TavernKeeper.Repo.Campaign.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "campaign_users" do
    field :campaign_id, Ecto.UUID
    field :pinned, :boolean, default: false
    field :status, :integer
    field :user_id, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:user_id, :campaign_id, :status, :pinned])
    |> validate_required([:user_id, :campaign_id, :status, :pinned])
  end
end
