defmodule TavernKeeper.Repo.Campaign.Discussion do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "campaign_discussions" do
    field :active, :boolean, default: false
    field :campaign_id, Ecto.UUID
    field :content, :string
    field :name, :string
    field :post_count, :integer
    field :user_id, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(discussion, attrs) do
    discussion
    |> cast(attrs, [:campaign_id, :user_id, :name, :content, :last_user_id, :post_count, :active])
    |> validate_required([
      :campaign_id,
      :user_id,
      :name,
      :content,
      :last_user_id,
      :post_count,
      :active
    ])
  end
end
