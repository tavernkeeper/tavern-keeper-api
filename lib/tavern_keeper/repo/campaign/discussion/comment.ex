defmodule TavernKeeper.Repo.Campaign.Discussion.Comment do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "campaign_discussion_comments" do
    field :campaign_discussion_id, Ecto.UUID
    field :content, :string
    field :user_id, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:campaign_discussion_id, :user_id, :content])
    |> validate_required([:campaign_discussion_id, :user_id, :content])
  end
end
