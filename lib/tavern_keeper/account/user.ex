defmodule TavernKeeper.Account.User do
  defstruct [:id, :email, :name]

  @type t :: %__MODULE__{id: Ecto.UUID.t(), email: String.t(), name: String.t()}

  alias TavernKeeper.Repo

  def find_user(id) do
    Repo.get(Repo.User, id)
  end

  defp cast(%Repo.User{} = user) do
    %__MODULE__{
      id: user.id,
      email: user.email,
      name: user.name
    }
  end
end
